##
## Create fortune(6) files from sources
##

SHELL		:= /bin/sh

sources	:= bjarne.tcpl3 bjarne.tcpl4 \
	   meyers.ec++ meyers.estl meyers.mec++ \
	   ms-quotes the-rules showerthoughts oath \

##
## Internals
##

objects	 = ${addsuffix .dat,${sources}}

all: build
help:
	@echo 'type `make` or `make build` to create missing `.dat` files'

build: ${objects}
veryclean: clean
distclean: clean
clean:
	-rm -f ${objects}

%.dat: %
	strfile $^ $@

.PHONY: all build help clean veryclean distclean
