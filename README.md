Fortunes
========

Enhance `fortunes` database with some custom quotes.

Current list of custom fortunes consists:

 - `bjarne.tcpl3`	: Quotes from the book "The C++ Programming Language, 3/e"
 - `bjarne.tcpl4`	: Quotes from the book "The C++ Programming Language, 4/e"
 - `meyers.ec++`	: Title items of the book "Effective C++"
 - `meyers.mec++`	: Title items of the book "More Effective C++"
 - `meyers.estl`	: Title items of the book "Effective STL"
 - `ms-quotes`		: Miscellaneous quotes of famous authors
 - `the-rules`		: The RULES, by @velominati
 - `showerthoughts`	: r/Showerthoughts, by Chris Wellons (null program)
 - `oath`		: The Programmer's Oath, by Robert C. (Uncle Bob) Martin
