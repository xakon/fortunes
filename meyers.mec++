Distinguish between pointers and references.
	--Scott Meyers, "More Effective C++", Item 1
%
Prefer C++-style casts.
	--Scott Meyers, "More Effective C++", Item 2
%
Never treat arrays polymorphically.
	--Scott Meyers, "More Effective C++", Item 3
%
Avoid gratuitous default constructors.
	--Scott Meyers, "More Effective C++", Item 4
%
Be wary of user-defined conversion functions.
	--Scott Meyers, "More Effective C++", Item 5
%
Distinguish between prefix and postfix forms of increment and
decrement operators.
	--Scott Meyers, "More Effective C++", Item 6
%
Never overload &&, ||, or ,.
	--Scott Meyers, "More Effective C++", Item 7
%
Understand the different meanings of new and delete.
	--Scott Meyers, "More Effective C++", Item 8
%
Use destructors to prevent resource leaks.
	--Scott Meyers, "More Effective C++", Item 9
%
Prevent resource leaks in constructors.
	--Scott Meyers, "More Effective C++", Item 10
%
Prevent exceptions from leaving destructors.
	--Scott Meyers, "More Effective C++", Item 11
%
Understand how throwing an exception differs from passing a parameter or
calling a virtual function.
	--Scott Meyers, "More Effective C++", Item 12
%
Catch exceptions by reference.
	--Scott Meyers, "More Effective C++", Item 13
%
Use exception specifications judiciously.
	--Scott Meyers, "More Effective C++", Item 14
%
Understand the costs of exception handling.
	--Scott Meyers, "More Effective C++", Item 15
%
Remember the 80-20 rule.
	--Scott Meyers, "More Effective C++", Item 16
%
Consider using lazy evaluation.
	--Scott Meyers, "More Effective C++", Item 17
%
Amortize the cost of expected computations.
	--Scott Meyers, "More Effective C++", Item 18
%
Understand the origin of temporary objects.
	--Scott Meyers, "More Effective C++", Item 19
%
Facilitate the return value optimization.
	--Scott Meyers, "More Effective C++", Item 20
%
Overload to avoid implicit type conversions.
	--Scott Meyers, "More Effective C++", Item 21
%
Consider using op= instead of stand-alone op.
	--Scott Meyers, "More Effective C++", Item 22
%
Consider alternative libraries.
	--Scott Meyers, "More Effective C++", Item 23
%
Understand the costs of virtual functions, multiple inheritance,
virtual base classes, and RTTI.
	--Scott Meyers, "More Effective C++", Item 24
%
Virtualizing constructors and non-member functions.
	--Scott Meyers, "More Effective C++", Item 25
%
Limiting the number of objects of a class.
	--Scott Meyers, "More Effective C++", Item 26
%
Requiring or prohibiting heap-based objects.
	--Scott Meyers, "More Effective C++", Item 27
%
Smart pointers.
	--Scott Meyers, "More Effective C++", Item 28
%
Reference counting.
	--Scott Meyers, "More Effective C++", Item 29
%
Proxy classes.
	--Scott Meyers, "More Effective C++", Item 30
%
Making functions virtual with respect to more than one object.
	--Scott Meyers, "More Effective C++", Item 31
%
Program in the future tense.
	--Scott Meyers, "More Effective C++", Item 32
%
Make non-leaf classes abstract.
	--Scott Meyers, "More Effective C++", Item 33
%
Understand how to combine C++ and C in the same program.
	--Scott Meyers, "More Effective C++", Item 34
%
Familiarize yourself with the language standard.
	--Scott Meyers, "More Effective C++", Item 35
