View C++ as a federation of languages.
	--Scott Meyers, "Effective C++", Item 1
%
Prefer consts, enums, and inlines to #defines.
	--Scott Meyers, "Effective C++", Item 2
%
Use const whenever possible.
	--Scott Meyers, "Effective C++", Item 3
%
Make sure that objects are initialized before they're used.
	--Scott Meyers, "Effective C++", Item 4
%
Know what functions C++ silently writes and calls.
	--Scott Meyers, "Effective C++", Item 5
%
Explicitly disallow the use of compiler-generated functions you do not want.
	--Scott Meyers, "Effective C++", Item 6
%
Declare destructors virtual in polymorphic base classes.
	--Scott Meyers, "Effective C++", Item 7
%
Prevent exceptions from leaving destructors.
	--Scott Meyers, "Effective C++", Item 8
%
Never call virtual functions during construction or destruction.
	--Scott Meyers, "Effective C++", Item 9
%
Have assignment operators return a reference to *this.
	--Scott Meyers, "Effective C++", Item 10
%
Handle assignment to self in operator=.
	--Scott Meyers, "Effective C++", Item 11
%
Copy all parts of an object.
	--Scott Meyers, "Effective C++", Item 12
%
Use objects to manage resources.
	--Scott Meyers, "Effective C++", Item 13
%
Think carefully about copying behavior in resource-managing classes.
	--Scott Meyers, "Effective C++", Item 14
%
Provide access to raw resources in resource-managing classes.
	--Scott Meyers, "Effective C++", Item 15
%
Use the same form in corresponding uses of new and delete.
	--Scott Meyers, "Effective C++", Item 16
%
Store newed objects in smart pointers in standalone statements.
	--Scott Meyers, "Effective C++", Item 17
%
Make interfaces easy to use correctly and hard to use incorrectly.
	--Scott Meyers, "Effective C++", Item 18
%
Treat class design as type design.
	--Scott Meyers, "Effective C++", Item 19
%
Prefer pass-by-reference-to-const to pass-by-value.
	--Scott Meyers, "Effective C++", Item 20
%
Don't try to return a reference when you must return an object.
	--Scott Meyers, "Effective C++", Item 21
%
Declare data members private.
	--Scott Meyers, "Effective C++", Item 22
%
Prefer non-member non-friend functions to member functions.
	--Scott Meyers, "Effective C++", Item 23
%
Declare non-member functions when type conversions should apply
to all parameters.
	--Scott Meyers, "Effective C++", Item 24
%
Consider support for a non-throwing swap.
	--Scott Meyers, "Effective C++", Item 25
%
Postpone variable definitions as long as possible.
	--Scott Meyers, "Effective C++", Item 26
%
Minimize casting.
	--Scott Meyers, "Effective C++", Item 27
%
Avoid returning "handles" to object internals.
	--Scott Meyers, "Effective C++", Item 28
%
Strive for exception-safe code.
	--Scott Meyers, "Effective C++", Item 29
%
Understand the ins and outs of inlining.
	--Scott Meyers, "Effective C++", Item 30
%
Minimize compilation dependencies between files.
	--Scott Meyers, "Effective C++", Item 31
%
Make sure public inheritance models "is-a".
	--Scott Meyers, "Effective C++", Item 32
%
Avoid hiding inherited names.
	--Scott Meyers, "Effective C++", Item 33
%
Differentiate between inheritance of interface and inheritance
of implementation.
	--Scott Meyers, "Effective C++", Item 34
%
Consider alternatives to virtual functions.
	--Scott Meyers, "Effective C++", Item 35
%
Never redefine an inherited non-virtual function.
	--Scott Meyers, "Effective C++", Item 36
%
Never redefine a function's inherited default parameter value.
	--Scott Meyers, "Effective C++", Item 37
%
Model "has-a" or "is-implemented-in-terms-of" through composition.
	--Scott Meyers, "Effective C++", Item 38
%
Use private inheritance judiciously.
	--Scott Meyers, "Effective C++", Item 39
%
Use multiple inheritance judiciously.
	--Scott Meyers, "Effective C++", Item 40
%
Understand implicit interfaces and compile-time polymorphism.
	--Scott Meyers, "Effective C++", Item 41
%
Understand the two meanings of typename.
	--Scott Meyers, "Effective C++", Item 42
%
Know how to access names in templatized base classes.
	--Scott Meyers, "Effective C++", Item 43
%
Factor parameter-independent code out of templates.
	--Scott Meyers, "Effective C++", Item 44
%
Use member function templates to accept "all compatible types."
	--Scott Meyers, "Effective C++", Item 45
%
Define non-member functions inside templates when type conversions are desired.
	--Scott Meyers, "Effective C++", Item 46
%
Use traits classes for information about types.
	--Scott Meyers, "Effective C++", Item 47
%
Be aware of template metaprogramming.
	--Scott Meyers, "Effective C++", Item 48
%
Understand the behavior of the new-handler.
	--Scott Meyers, "Effective C++", Item 49
%
Understand when it makes sense to replace new and delete.
	--Scott Meyers, "Effective C++", Item 50
%
Adhere to convention when writing new and delete.
	--Scott Meyers, "Effective C++", Item 51
%
Write placement delete if you write placement new.
	--Scott Meyers, "Effective C++", Item 52
%
Pay attention to compiler warnings.
	--Scott Meyers, "Effective C++", Item 53
%
Familiarize yourself with the standard library, including TR1.
	--Scott Meyers, "Effective C++", Item 54
%
Familiarize yourself with Boost.
	--Scott Meyers, "Effective C++", Item 55
